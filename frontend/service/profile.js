import axios from 'axios'

export default {
  async createProfile(data) {
    const res = await axios({
      method: "post",
      url: `http://localhost:3030/profile`,
      headers: {
        "Content-Type": "application/json"
      },
      data
    })
    return res.data
  },
  async getProfile({ fieldName, value }) {
    const res = await axios({
      method: "get",
      url: `http://localhost:3030/profile?${fieldName}=${value}`,
      headers: {
        "Content-Type": "application/json"
      }
    })
    return res.data
  },
  async changePassword(id, data) {
    const res = await axios({
      method: "put",
      url: `http://localhost:3030/profile/${id}?module=changePassword`,
      headers: {
        "Content-Type": "application/json"
      },
      data
    })
    return res.data
  }
}
