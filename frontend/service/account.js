import axios from 'axios'

export default {
  async createAccount(data) {
    console.log('ser', data)
    return await axios({
      method: "post",
      url: `http://localhost:3030/account`,
      headers: {
        "Content-Type": "application/json"
      },
      data
    })
  },
  async getAccount({ fieldName, value }) {
    const res = await axios({
      method: "get",
      url: `http://localhost:3030/account?${fieldName}=${value}`,
      headers: {
        "Content-Type": "application/json"
      }
    })
    return res.data
  }
}
