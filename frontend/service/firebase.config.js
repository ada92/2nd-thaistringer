import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/database'

var firebaseConfig = {
    apiKey: "AIzaSyChuUF8r5m4bDs0_J9WOxOoChWZpmUvFgs",
    authDomain: "test-4d6f1.firebaseapp.com",
    databaseURL: "https://test-4d6f1.firebaseio.com",
    projectId: "test-4d6f1",
    storageBucket: "test-4d6f1.appspot.com",
    messagingSenderId: "84583896736",
    appId: "1:84583896736:web:8e7471d7745d7a21"
};

// Initialize Firebase
if (!firebase.apps.length) { firebase.initializeApp(firebaseConfig) }
export const auth = firebase.auth()
export const DB = firebase.database()
export const StoreDB = firebase.firestore()
export const storage = firebase.storage()
export default firebase
