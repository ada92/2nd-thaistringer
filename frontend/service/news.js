import axios from 'axios'

export default {
    async createNews(data) {
        let formData = new FormData();
        formData.append("file", data.file);
        formData.append("title", data.title);
        return await axios({
            method: 'post',
            url: "http://localhost:3110/uploads",
            headers: {
                "Content-Type": "multipart/form-data"
            },
            data: formData
        })
    },

}