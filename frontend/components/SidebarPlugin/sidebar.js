import Vue from 'vue';

const SidebarStore = {
  showSidebar: false,
  displaySidebar(value) {
    this.showSidebar = value;
  }
};

Vue.mixin({
  data() {
    return {
      sidebarStore: SidebarStore
    };
  }
});

if (!Vue.prototype.hasOwnProperty("$sidebar")) {
  Object.defineProperty(Vue.prototype, "$sidebar", {
    get() { return this.$root.sidebarStore; }
  })
}

// const SidebarStore = {
//   showSidebar: false,
//   sidebarLinks: [],
//   isMinimized: false,
//   displaySidebar(value) {
//     this.showSidebar = value;
//   },
// };

// Vue.mixin({
//   data() {
//     return {
//       sidebarStore: SidebarStore
//     };
//   }
// });

// Object.defineProperty(Vue.prototype, "$sidebar", {
//   get() {
//     return this.$root.sidebarStore;
//   }
// });

// const SidebarPlugin = {
//   install(Vue, options) {
//     if (options && options.sidebarLinks) {
//       SidebarStore.sidebarLinks = options.sidebarLinks;
//     }
//     Vue.mixin({
//       data() {
//         return {
//           sidebarStore: SidebarStore
//         };
//       }
//     });

//     Object.defineProperty(Vue.prototype, "$sidebar", {
//       get() {
//         return this.$root.sidebarStore;
//       }
//     });
//     Vue.component("side-bar", Sidebar);
//     Vue.component("sidebar-item", SidebarItem);
//   }
// };

// export default SidebarPlugin;
