
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Thai Stringers',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS*/
  css: [
    '~/assets/admin.scss',
    '~/assets/broadcaster.scss',
    '~/assets/material-dashboard.scss',
    '~/assets/table.scss',
    '~/assets/thaistring-style.scss',
    '~/assets/home-style.scss',
    '~/assets/register-style.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/service/firebase.config.js',
    '~/plugins/vue-material',
    '~/plugins/global-component',
    '~/plugins/vue-date-fns',
    '~/plugins/vue-vuelidate',
    '~/components/SidebarPluginPro/index',
    '~/components/NotificationPlugin/index',
    '~/components/Wizard/throttle',
    '~/plugins/axios'
  ],
  /*
  ** Router
  */
  router: {
    base: '/',
    linkActiveClass: 'active'
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/onesignal',
    '@nuxtjs/pwa',
  ],
  oneSignal: {
    init: {
      appId: '5dcec52e-c083-4909-aae6-1c5dd7846458',
      allowLocalhostAsSecureOrigin: true,
      welcomeNotification: {
        disable: true
      }
    }
  },
  pwa: {
    manifest: {
      name: 'ThaiStringer',
      lang: 'en'
    }
  },
  axios: {
    proxy: true // Can be also an object with default options
  },
  // proxy: {
  //   '/': 'http://localhost:3110',
  // },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  env: {
  }
}
