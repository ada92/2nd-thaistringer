import Vue from 'vue'
// import SideBar from "../components/SidebarPlugin"
import VueMaterial from 'vue-material'
import VueMoment from 'vue-moment'
import Chartist from "chartist"

import VuePlyr from "vue-plyr";
import "vue-plyr/dist/vue-plyr.css";

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import 'vue-select/dist/vue-select.css'
// import '../assets/meterial-style.scss'

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'



Vue.prototype.$Chartist = Chartist;
// Vue.use(SideBar)
Vue.use(VueFormWizard)
Vue.use(VueMaterial)
Vue.use(VueMoment)
Vue.use(VuePlyr);
