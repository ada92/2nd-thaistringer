export default function ({ $axios, redirect }) {
  $axios.setHeader('Content-Type', "multipart/form-data")
  
  $axios.onResponse(res => {
    console.log(res)
  })
  
  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  })
}
