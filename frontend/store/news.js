import uuidv4 from "uuid"
import { DB, storage } from '../service/firebase.config'
import createnews from '../service/news'

var uid = uuidv4()

export const state = () => ({
    news: null,
    media: null
})

export const mutations = {
    // CREATE_NEWS(state, news) {
    //     state.news = news
    // },
    ADD_NEWS(state, news) {
        state.news = news
    }
}

export const actions = {
    // async createNews({ commit }, data) {
    //     const resNews = await createnews.createNews(data)
    //     commit(CREATE_NEWS, resNews)
    // },
    async addNews({ commit }, newsData) {
        var database = DB.ref("news").child(uid);
        var thumbnail_file = storage.ref().child(this.uid);
        var databaseMedia = DB.ref("media");
        var current_time = new Date()
        await database.set({
            create_datetime: current_time.toString(),
            id: uid,
            title: newsData.title,
            category: newsData.category,
            short_description: newsData.short_description,
            full_description: newsData.full_description,
            tag: newsData.tag,
            genre: newsData.genre,
            location: newsData.location
        })
        commit(ADD_NEWS, newsData)
    }
}