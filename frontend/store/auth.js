import moment from 'moment'
import feathersClient from './feathers-client'
import firebase, { storage } from '../service/firebase.config'
import accountService from '../service/account'
import profileService from '../service/profile'

// action constants
export const REGISTER = 'register'
export const LOGIN = 'login'
export const LOGOUT = 'logout'
export const CHECK_DUPLICATE_EMAIL = 'checkDuplicateEamil'

// mutation constants
export const SET_AUTH = "setUser";
export const PURGE_AUTH = "logOut";
export const SET_ERROR = "setError";

export const state = () => ({
  errors: null,
  user: {},
  isAuthenticated: false,
})

export const actions = {
  async [REGISTER]({ commit }, { accountData, profileData }) {
    return new Promise(async (resolve, reject) => {
      const dateFile = moment().format('DDMMYYYY')
      var birthday = moment(`${profileData.birthday}`).format('DDMMYYYY')

      const resAccount = await accountService.createAccount(accountData)

      const idCardFilePath = `${dateFile}/${profileData.idCardFile.name}`
      const bookBankFilePath = `${dateFile}/${profileData.bookBankFile.name}`
      const photoPath = `${dateFile}/${profileData.photo.name}`

      var blobIdCardFile = new Blob([profileData.photo], { type: "image/png" });
      var blobBookBankFile = new Blob([profileData.photo], { type: "image/png" });
      var blobPhoto = new Blob([profileData.photo], { type: "image/png" });

      const storageRef = storage.ref(`${resAccount.data.id}`)

      await storageRef.child(`${idCardFilePath}`).put(blobIdCardFile)
      await storageRef.child(`${bookBankFilePath}`).put(blobBookBankFile)
      await storageRef.child(`${photoPath}`).put(blobPhoto)

      const profileObj = {
        ...profileData,
        idCardFile: `${resAccount.data.id}/${idCardFilePath}`,
        bookBankFile: `${resAccount.data.id}/${bookBankFilePath}`,
        photo: `${resAccount.data.id}/${photoPath}`,
        birthday,
        accountId: resAccount.data.id
      }
      const resProfile = await profileService.createProfile(profileObj)
      resolve({ account: resAccount.data, profile: resProfile.data })
      commit(SET_AUTH, { account: resAccount.data, profile: resProfile.data })
    }, error => {
      commit(SET_ERROR, error)
      reject(error);
    })
  },
  async [LOGIN]({ commit }, { email, password }) {
    return new Promise(async (resolve, reject) => {
      const account = await feathersClient.authenticate({
        strategy: 'local',
        email,
        password
      })
      const profile = await profileService.getProfile({ fieldName: 'accountId', value: account.account.id })
      resolve({ account, profile: profile.data[0] })
      commit(SET_AUTH, { account, profile: profile.data[0] })
    }, error => {
      commit(SET_ERROR, error)
      reject(error);
    })
  }
}

export const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
  }
}