import ProfileService from '../service/profile'

export const state = () => ({
  data: {}
})

export const mutation = {
  setProfile(state, profile) {
    state.data = profile
  }
}

export const actions = {
  async getProfile({ commit }, id) {
    const profile = await ProfileService.getProfile({ fieldName: 'accountId', value: id })
    commit('setProfile', profile.data[0])
  }
}
