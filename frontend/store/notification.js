import notificationService from '../service/notification'

export const state = () => ({
  message: null,
  searched: null,
  keyword: null
})

export const mutations = {
  GET_MESSAGE(state, message) {
    state.message = message
  },
  SEARCH_MESSAGE(state, data) {
    state.searched = data.message
    state.keyword = data.keyword
  }
}

export const actions = {
  async loadMessage({ commit }, id) {
    const res = await notificationService.getMessage(id)
    commit('GET_MESSAGE', res)
  },
  async updateStatusMessage({ commit }, data) {
    const res = await notificationService.updateStatus(data)
    if (this.state.keyword) {
      const search = searchName(res, this.state.keyword)
      commit('SEARCH_MESSAGE', {
        message: search,
        keyword: this.state.keyword
      })
      commit('GET_MESSAGE', res)
    } else {
      commit('GET_MESSAGE', res)
    }
  },
  async deleteMessage({ commit }, data) {
    const res = await notificationService.deleteMessage(data);
    commit('GET_MESSAGE', res)
    commit('SEARCH_MESSAGE', {
      message: res,
      keyword: this.state.keyword
    });
    return
  },
  async searchByName({ commit }, keyword) {
    const res = searchName([...this.state.notification.message], keyword);
    commit('SEARCH_MESSAGE', {
      message: res,
      keyword
    })
  }
};

const searchName = (message, keyword) => {
  return message.filter(item => {
    return item.sender.toString().toLowerCase().includes(keyword.toString().toLowerCase())
  })
}
