"use strict";

var _services = _interopRequireDefault(require("./services"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var feathers = require('@feathersjs/feathers');

var express = require('@feathersjs/express');

var cors = require('cors');

var app = express(feathers());
app.use(cors({
  origin: true
}));
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.configure(express.rest());
app.use(express.errorHandler());
app.configure(_services["default"]);
var server = app.listen(3032);
server.on('listening', function () {
  return console.log('Feathers REST API started at http://localhost:3032');
});