const postgreSql = require('./notification/notification.service');
const accountPgSql = require('./account/account.service');
const profiletPgSql = require('./profile/profile.service');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(postgreSql);
  app.configure(accountPgSql);
  app.configure(profiletPgSql);
};
